using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers.Admin
{
    [Route("admin/[controller]/[action]")]
    public class PreferenceController : Controller
    {
        private IRepository<Preference> _preferenceRepository;
        public PreferenceController(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        public async Task<IActionResult> Index()
        {
            IEnumerable<Preference> preferences;
            try
            {
                preferences = await _preferenceRepository.GetAllAsync();
                if (preferences == null) new List<Preference>();
            }
            catch (Exception)
            {
                preferences = new List<Preference>();
            }

            return View(preferences);
        }


        // GET: Preferences/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Preferences/Create       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(Preference request)
        {
            if (!ModelState.IsValid) return View(request);

            var preference = new Preference()
            {
                Name = request.Name
            };

            await _preferenceRepository.AddAsync(preference);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preference = await _preferenceRepository.GetByIdAsync(id.Value);

            if (preference == null)
            {
                return NotFound();
            }

            return View(preference);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(Preference request)
        {
            if (!ModelState.IsValid) return View(request);

            var preference = await _preferenceRepository.GetByIdAsync(request.Id);

            if (preference == null)
                return NotFound();

            preference.Name = request.Name;

            await _preferenceRepository.UpdateAsync(preference);

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preference = await _preferenceRepository.GetByIdAsync(id.Value);

            if (preference == null)
            {
                return NotFound();
            }

            return View(preference);
        }

          public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var preference = await _preferenceRepository.GetByIdAsync(id.Value);

            if (preference == null)
            {
                return NotFound();
            }

            return View(preference);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var preference = await _preferenceRepository.GetByIdAsync(id);

            if (preference == null)
                return NotFound();

            await _preferenceRepository.DeleteAsync(preference);

            return RedirectToAction(nameof(Index));
        }
    }
}