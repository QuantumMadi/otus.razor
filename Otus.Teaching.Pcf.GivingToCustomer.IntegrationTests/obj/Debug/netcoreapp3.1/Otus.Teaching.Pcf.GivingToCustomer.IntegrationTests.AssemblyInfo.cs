//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Reflection;

[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("Otus.Teaching.Pcf.GivingToCustomer.Core, Version=1.0.0.0, Culture=neutral, Public" +
    "KeyToken=null", "/home/madi/DotnetProjects/razor.homework/Otus.Teaching.Pcf.GivingToCustomer.Core", "Otus.Teaching.Pcf.GivingToCustomer.Core.csproj", "0")]
[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("Otus.Teaching.Pcf.GivingToCustomer.DataAccess, Version=1.0.0.0, Culture=neutral, " +
    "PublicKeyToken=null", "/home/madi/DotnetProjects/razor.homework/Otus.Teaching.Pcf.GivingToCustomer.DataA" +
    "ccess", "Otus.Teaching.Pcf.GivingToCustomer.DataAccess.csproj", "0")]
[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("Otus.Teaching.Pcf.GivingToCustomer.Integration, Version=1.0.0.0, Culture=neutral," +
    " PublicKeyToken=null", "/home/madi/DotnetProjects/razor.homework/Otus.Teaching.Pcf.GivingToCustomer.Integ" +
    "ration", "Otus.Teaching.Pcf.GivingToCustomer.Integration.csproj", "0")]
[assembly: Microsoft.AspNetCore.Mvc.Testing.WebApplicationFactoryContentRootAttribute("Otus.Teaching.Pcf.GivingToCustomer.WebHost, Version=1.0.0.0, Culture=neutral, Pub" +
    "licKeyToken=null", "/home/madi/DotnetProjects/razor.homework/Otus.Teaching.Pcf.GivingToCustomer.WebHo" +
    "st", "Otus.Teaching.Pcf.GivingToCustomer.WebHost.csproj", "0")]
[assembly: System.Reflection.AssemblyCompanyAttribute("Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests")]
[assembly: System.Reflection.AssemblyConfigurationAttribute("Debug")]
[assembly: System.Reflection.AssemblyFileVersionAttribute("1.0.0.0")]
[assembly: System.Reflection.AssemblyInformationalVersionAttribute("1.0.0")]
[assembly: System.Reflection.AssemblyProductAttribute("Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests")]
[assembly: System.Reflection.AssemblyTitleAttribute("Otus.Teaching.Pcf.GivingToCustomer.IntegrationTests")]
[assembly: System.Reflection.AssemblyVersionAttribute("1.0.0.0")]

// Generated by the MSBuild WriteCodeFragment class.

