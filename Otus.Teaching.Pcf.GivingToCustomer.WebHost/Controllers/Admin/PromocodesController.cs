using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Repositories;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;

namespace Otus.Teaching.Pcf.GivingToCustomer.WebHost.Controllers.Admin
{

    [Route("admin/[controller]/[action]")]
    public class PromocodeController : Controller
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;

        public PromocodeController(IRepository<PromoCode> promoCodesRepository)
        {
            _promoCodesRepository = promoCodesRepository;
        }

         public async Task<IActionResult> Index()
        {
            IEnumerable<PromoCode> promoCodes;
            try
            {
                promoCodes = await _promoCodesRepository.GetAllAsync();
                if (promoCodes == null) new List<PromoCode>();
            }
            catch (Exception)
            {
                promoCodes = new List<PromoCode>();
            }        

            return View(promoCodes);
        }

        // GET: PromoCodes/Details/5
        public async Task<IActionResult> Details(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promoCode = await _promoCodesRepository.GetByIdAsync(id.Value);

            if (promoCode == null)
            {
                return NotFound();
            }

            return View(promoCode);
        }

        // GET: PromoCodes/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: PromoCodes/Create       
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(PromoCode request)
        {
            if (!ModelState.IsValid) return View(request);

            var promoCode = new PromoCode()
            {
                ServiceInfo = request.ServiceInfo,
                BeginDate = request.BeginDate,
                Code = request.Code,
                EndDate = request.EndDate,
            };

            await _promoCodesRepository.AddAsync(promoCode);

            return RedirectToAction(nameof(Index));
        }

        // GET: PromoCodes/Edit/5
        public async Task<IActionResult> Edit(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promoCode = await _promoCodesRepository.GetByIdAsync(id.Value);

            if (promoCode == null)
            {
                return NotFound();
            }

            return View(promoCode);
        }

        // POST: PromoCodes/Edit/5        
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(PromoCode request)
        {
            if (!ModelState.IsValid) return View(request);

            var promoCode = await _promoCodesRepository.GetByIdAsync(request.Id);

            if (promoCode == null)
                return NotFound();

            promoCode.ServiceInfo = request.ServiceInfo;
            promoCode.BeginDate = request.BeginDate;
            promoCode.Code = request.Code;
            promoCode.EndDate = request.EndDate;

            await _promoCodesRepository.UpdateAsync(promoCode);

            return RedirectToAction(nameof(Index));
        }

        // GET: PromoCodes/Delete/5
        public async Task<IActionResult> Delete(Guid? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var promoCode = await _promoCodesRepository.GetByIdAsync(id.Value);

            if (promoCode == null)
            {
                return NotFound();
            }

            return View(promoCode);
        }

        // POST: PromoCodes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(Guid id)
        {
            var promoCode = await _promoCodesRepository.GetByIdAsync(id);

            if (promoCode == null)
                return NotFound();

            await _promoCodesRepository.DeleteAsync(promoCode);

            return RedirectToAction(nameof(Index));
        }

        private bool PromoCodeExists(Guid id)
        {
            var promoCode = _promoCodesRepository.GetByIdAsync(id).Result;

            return (promoCode != null);
        }
    }
}